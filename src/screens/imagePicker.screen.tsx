import React, { useState } from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
// import ImagePicker from 'react-native-image-picker';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';

// More info on all the options is below in the API Reference... just some common use cases shown here
const options = {
    mediaTypes: 'Images',
    quality: 0.1,
    base64: true,
};

export default function ImagePicker() {
    if (Constants.platform.ios) {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
        }
    }
    const [picture, setPicture] = useState(null);

    return (
        <View style={styles.container}>
            <Text>Image Picker</Text>
            <Button
                title={'Take Photo'}
                onPress={}
            >Launch Camera</Button>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
