import React from 'react';
import { Button, Image, View } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as ImageManipulator from 'expo-image-manipulator';

export default class ImagePickerComponent extends React.Component {
    state = {
        image: null,
        hasCameraPermission: null,
    };

    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }

    render() {
        let { image } = this.state;

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Button
                    title="Take Picture"
                    onPress={this._pickImage}
                />
                {image &&
                    <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
            </View>
        );
    }

    _pickImage = async () => {
        const result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: false,
            aspect: [4, 3],
            quality: 1,
            base64: true,
        });

        const manipResult = await ImageManipulator.manipulateAsync(
            result.uri,
            [{ resize: { height: 640, width: 480 } }],
            { compress: .5, format: ImageManipulator.SaveFormat.JPEG, base64: true }
        );

        if (!result.cancelled) {
            // @ts-ignore
            this.setState({ image: manipResult.uri });
        }
    };
}
